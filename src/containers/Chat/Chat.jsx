import React, { useState, useEffect, useCallback } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
  getMessages,
  sendMessage,
  editMessage,
  deleteMessage,
  likeMessage,
  setEditingMessage,
  hideModal,
} from './actions/messages';
import { Spinner } from '../../components/Spinner';
import { container } from './chat.module.scss';
import { MessageArea } from '../../components/MessageArea';
import { ChatHeader } from '../../components/ChatHeader';
import { Footer } from '../../components/Footer';
import { InputMessage } from '../../components/InputMessage';
import { Header } from '../../components/Header';
import { EditMessageModal } from '../../components/EditMessageModal';

const Chat = ({
  isLoaded,
  isEditModalShown,
  messages,
  getMessages: getAllMessages,
  sendMessage,
  editMessage,
  deleteMessage,
  likeMessage,
  setEditingMessage,
  editingMessage,
  hideModal,
}) => {
  const currentUserId = 'userId';
  const [messageText, setMessageText] = useState('');
  const [editingMessageText, setEditingMessageText] = useState('');

  const onUpButton = useCallback(
    (event) => {
      if (event.key !== 'ArrowUp') {
        return;
      }
      setEditingMessage();
    },
    [setEditingMessage]
  );

  useEffect(() => {
    getAllMessages();

    document.addEventListener('keydown', onUpButton);
    return () => {
      document.removeEventListener('keydown', onUpButton);
    };
  }, [getAllMessages, onUpButton]);

  return (
    <div className={container}>
      {!isLoaded ? (
        <Spinner />
      ) : (
        <>
          <Header />
          <ChatHeader messages={messages} />
          <MessageArea
            messages={messages}
            currentUserId={currentUserId}
            setEditingMessage={setEditingMessage}
            likeMessage={likeMessage}
            deleteMessage={deleteMessage}
            setEditingMessageText={setEditingMessageText}
          />
          <InputMessage
            sendMessage={sendMessage}
            editingMessage={editingMessage}
            messageText={messageText}
            setMessageText={setMessageText}
            editMessage={editMessage}
          />
          {isEditModalShown ? (
            <EditMessageModal
              editMessage={editMessage}
              hideModal={hideModal}
              editingMessageText={editingMessageText}
              setEditingMessageText={setEditingMessageText}
              editingMessage={editingMessage}
            />
          ) : (
            ''
          )}
          <Footer />
        </>
      )}
    </div>
  );
};

const mapStateToProps = (rootState) => ({
  messages: rootState.messages.messages,
  isLoaded: rootState.messages.isLoaded,
  editingMessage: rootState.messages.editingMessage,
  isEditModalShown: rootState.messages.isEditModalShown,
});

const actions = {
  getMessages,
  sendMessage,
  editMessage,
  deleteMessage,
  likeMessage,
  setEditingMessage,
  hideModal,
};

const mapDispatchToProps = (dispatch) => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
