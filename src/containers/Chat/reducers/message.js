import { SEND_MESSAGE } from '../actions/actionsTypes';



const sendMessage = (state, message) => ({
  ...state,
  ...message,
});

export default (state = initialState, action) => {
  switch (action.type) {
    case SEND_MESSAGE:
      return sendMessage(state, action.message);
    default:
      return state;
  }
};
