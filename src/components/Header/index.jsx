import React from 'react';

import styles from './styles.module.scss'
import logo from '../../assets/logo/logo.png';

export const Header = () => {
  return (
    <div className={styles.container}>
      <img src={logo} alt="Logo" className={styles.image} />
    </div>
  );
};
