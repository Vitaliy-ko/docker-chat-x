import React from 'react';
import styles from './style.module.scss';

export const InputMessage = ({ messageText, setMessageText, sendMessage }) => {
  const inputMessageHandler = (msg) => {
    setMessageText(msg);
  };

  const sendMessageHandler = (messageText) => {
    if (!messageText) {
      return;
    }
    sendMessage(messageText);
    setMessageText('');
  };

  return (
    <div className={styles.container}>
      <textarea
        tabIndex="1"
        className={styles.inputMessage}
        onChange={(e) => inputMessageHandler(e.target.value)}
        value={messageText}
        placeholder="Enter your message "
      ></textarea>

      <button
        type="submit"
        className={styles.sendMessage}
        onClick={() => sendMessageHandler(messageText)}
        tabIndex="2"
      >
        Send
      </button>
    </div>
  );
};
